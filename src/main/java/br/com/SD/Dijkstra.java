package br.com.SD;

import java.util.ArrayList;

import org.apache.thrift.TException;

import br.com.SD.Aresta;
import br.com.SD.Vertice;

public class Dijkstra {

	static float min;
	static int next = 0;
	static Handler handler;
	static Vertice vertice;
	
	
	
	public static void main(String args[]){
		
		handler = new Handler(null);
		vertice = new Vertice();
		
		System.out.println("Testando Dijkstra");
		
		ArrayList<Aresta> listaArestas;
		try {
			listaArestas = (ArrayList<Aresta>) handler.listaTodasArestas();
			ArrayList<Vertice> listaVertices = (ArrayList<Vertice>) handler.listaTodosVertices();

			
			menorDistancia(listaArestas,40,4,9,listaVertices);
			
		} catch (TException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	} 
	
	public static float[][] converte(ArrayList<Aresta> listaArestas, int tamanho){
		float[][] grapho = new float[50][50];
		
		for(Aresta aresta : listaArestas){
			grapho[aresta.getNomeVertice1()][aresta.getNomeVertice2()] = (float) aresta.getPeso();
		}
		
		return grapho;
	}
	
	
	
	public static void menorDistancia(ArrayList<Aresta> listaArestas, int tamanho, int tag1, int tag2, ArrayList<Vertice> array){
	      
		float[][] grapho = converte(listaArestas, array.size());
		
	       float distancia[] = new float[tamanho];
	       int visitados[] = new int[tamanho];
	       int pre[] = new int[tamanho+1];
	       float matriz[][] = new float[tamanho][tamanho];
	       matriz = grapho;
	       for(int i=0; i<=tamanho; i++){
	           for(int j=0; j<=tamanho; j++){
	               if(matriz[i][j]<=0){
	                   matriz[i][j]=999;
	               }
	           }
	       }
	       for(int i=0; i<tamanho; i++){     
	            distancia[i] = matriz[tag1][i+1];
	       }
	       
	       visitados[tag1-1] = 1;
	       
	       for(int i=0; i<tamanho; i++){
	//	           min=999;
	//	           for(int j=0; j<tamanho; j++){
	//	               if(min>distancia[j] && visitados[j]!=1){
	//	                   min = distancia[j];
	//	                   next=j;
	//	                   
	//	               }
	//	           }
	   
	   
		   min=999;
		   for(int j=0; j<tamanho; j++){
			   try {
				vertice = handler.buscaVertice(j);
			} catch (TException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
				}
		           if(min>distancia[vertice.getNome()] && visitados[vertice.getNome()]!=1){
		               min = distancia[j];
		               next=j;
		               
		           }
		       }
		       
		   
		    visitados[next] = 1;
		    
		    
		    for(int c=0; c<tamanho; c++){
		           if(visitados[c]!=1){
		               if(min+matriz[next+1][c+1]<distancia[c]){
		                   distancia[c]= min+matriz[next+1][c+1];
		                   pre[c] = next;
		               }
		               
		           }
		    }
		   
		    }       
		    int j;
		    if(distancia[tag2-1]==-1 || distancia[tag2-1]==999){
		    	System.out.println("\nCaminho: "+array.get(tag2-1).getNome()+" Distância: "+999);
		}
		else if(distancia[tag2-1]>0 && distancia[tag2-1]<999){
			//System.out.println("\nCaminho: "+array.get(tag2-1).getNome()+" Distância: "+distancia[tag2-1]);
			for(Vertice v : array){
				if(v.getNome() == tag2)
					System.out.println("\nCaminho: de " + tag1 + " até " + v.getNome()+" Distância: "+distancia[tag2-1]);    
			}
		}
		j=tag2-1;
		do{
		        j=pre[j];
		        if(j!=0){
		            System.out.println(" <- "+array.get(j).getNome());
		        }
		}while(j!=0);
		
		System.out.println(" <- "+array.get(tag1-1).getNome());
		
		for(int i=0; i<tamanho; i++){     
		    
		    distancia[i] = 0;
		    visitados[i] = 0;
		    pre[i]=0;
		    
		}
	       
	       
	  }   
	
}

