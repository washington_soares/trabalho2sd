package br.com.SD;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.thrift.TException;

import br.com.SD.RaftCopyCat.CommandCriaVertice;
import io.atomix.copycat.client.CopycatClient;
import br.com.SD.Aresta;
import br.com.SD.GrafoService;
import br.com.SD.Vertice;

public class Handler implements GrafoService.Iface {

	private List<Aresta> listaDeArestas;
	private Lock lock = new ReentrantLock();
	static float min;
	static int next = 0;
	CopycatClient ccClient;
	
	public Handler(CopycatClient client){
//		try {
//			
//			//listaDeVertices = this.listaTodosVertices();
//			//istaDeArestas = this.listaTodasArestas();
//		} catch (TException e) {
//			e.printStackTrace();
//		}
		System.out.println("");
		this.ccClient = client;
		
	}
	
	@Override
	public Vertice buscaVertice(int nomeVertice) throws TException {
		Vertice vertice = new Vertice();
		List<Vertice> listaV= new ArrayList<Vertice>();
		try{
			lock.lock();
			// Região Crítica
			// Buscar o vertice na base de dados ao invés de colcar em listas na memória
			
			GrafoService.Client client = Servidor.buscaServidor(nomeVertice);
			
			if(client == null){
				listaV = Servidor.carregarVertices(Servidor.mapeamento.getPathBD());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
				for(Vertice v : listaV){
					if(nomeVertice == v.getNome()){
						System.out.println("Vertice: " + vertice.getNome());
						return v;
					}
				}
			}else{
				return client.buscaVertice(nomeVertice);
			}
			
			
			
		}finally{
		
			lock.unlock();
		}
		
		return vertice;
	}

	@Override
	public Vertice criaVertice(Vertice vertice) throws TException {
		List<Vertice> listaV= new ArrayList<Vertice>();
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.buscaServidor(vertice.getNome());
			
			if(client == null){
				CommandCriaVertice c = new CommandCriaVertice(vertice);
				this.ccClient.submit(c);
			}else{
				return client.criaVertice(vertice);
			}
						
			
			
		}finally{
			lock.unlock();
		}
		
		return vertice;
		
	}

	@Override
	public void deletaVertice(int nomeVertice) throws TException {
		List<Vertice> listaV= new ArrayList<Vertice>();
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.buscaServidor(nomeVertice);
			
			if(client == null){
				listaV = Servidor.carregarVertices(Servidor.mapeamento.getPathBD());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			}else{
				client.deletaVertice(nomeVertice);
			}
			
			for(Vertice v : listaV){
				if(v.getNome() == nomeVertice){
					listaV.remove(v);
					deletaAresta3(nomeVertice);
					Servidor.criaArquivoVertice(listaV,Servidor.mapeamento.getPathBD());
					//Servidor.criaArquivoAresta(listaDeArestas);
					return;
				}
			}
		}finally{
			lock.unlock();
		}
		
		System.out.println("Vertice não encontrado!");
		
	}

	@Override
	public Aresta buscaAresta(int nomeAresta1, int nomeAresta2) throws TException {
		List<Aresta> listaA= new ArrayList<Aresta>();
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.buscaServidor(nomeAresta1);
			
			if(client == null){
				listaA = Servidor.carregarArestas(Servidor.mapeamento.getPathBDAresta());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			}else{
				return client.buscaAresta(nomeAresta1,nomeAresta2);
			}
			
			for(Aresta a : listaA){
				if((a.getNomeVertice1() == nomeAresta1 && a.getNomeVertice2() == nomeAresta2)  || (a.getNomeVertice1() == nomeAresta2 && a.getNomeVertice2() == nomeAresta1)){
					return a;
				}
			}
		
		}finally{
			lock.unlock();
		}
		return new Aresta();
		
		
	}	

	@Override
	public Aresta criaAresta(Aresta aresta) throws TException {
		
		try{
			lock.lock();
			
			
			Vertice v = new Vertice();
			Vertice v2 = new Vertice();
			GrafoService.Client client;
			//Aresta a = buscaAresta(aresta.getNomeVertice1(), aresta.getNomeVertice2());
			
			
			client = Servidor.buscaServidor(aresta.getNomeVertice1());
			
			if(client == null){
				v = buscaVertice(aresta.getNomeVertice1());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			}else{
				return  client.criaAresta(aresta);
			}
			
			//Vertice v = buscaVertice(aresta.getNomeVertice1());
			if(v.getNome() != 0){
				client = Servidor.buscaServidor(aresta.getNomeVertice2());
				
				if(client == null){
					v2 = buscaVertice(aresta.getNomeVertice2());
					System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
				}else{
					v2 = client.buscaVertice(aresta.getNomeVertice2());
				}

				
				if( v2.getNome() != 0){
					listaDeArestas = Servidor.carregarArestas(Servidor.mapeamento.getPathBDAresta());
					listaDeArestas.add(aresta);
					Servidor.criaArquivoAresta(listaDeArestas, Servidor.mapeamento.getPathBDAresta());
					client.criaAresta2(aresta);
					return aresta;
				}else{
					System.out.println("Não existe o vertice: " + aresta.getNomeVertice2() + " no grafo");
					return null;
				}
			}else{
				System.out.println("Não existe o vertice: " +aresta.getNomeVertice1() + " no grafo");
				return null;
			}
//			Vertice v = new Vertice();
//			GrafoService.Client client;
//			Aresta a = this.buscaAresta(aresta.getNomeVertice1(), aresta.getNomeVertice2());
//			if( a.getNomeVertice1() != 0){
//				System.out.println("Já existe esta aresta!");
//				return null;
//			}else{
//				client = Servidor.buscaServidor(aresta.getNomeVertice1());
//				
//				if(client == null){
//					v = buscaVertice(aresta.getNomeVertice1());
//					System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
//				}else{
//					client.buscaVertice(aresta.getNomeVertice1());
//				}
//				
//				//Vertice v = buscaVertice(aresta.getNomeVertice1());
//				if(v.getNome() != 0){
//					client = Servidor.buscaServidor(aresta.getNomeVertice2());
//					
//					if(client.getInputProtocol() == null){
//						v = buscaVertice(aresta.getNomeVertice2());
//						System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
//					}else{
//						client.buscaVertice(aresta.getNomeVertice2());
//					}
//					
//					
//					
//					if( v.getNome() != 0){
//						listaDeArestas.add(aresta);
//						Servidor.criaArquivoAresta(listaDeArestas);
//					}else
//						System.out.println("Não existe o vertice: " + aresta.getNomeVertice2() + " no grafo");
//				}else
//					System.out.println("Não existe o vertice: " +aresta.getNomeVertice1() + " no grafo");
//			}
		}finally{
			lock.unlock();
		}
		
	}
	
	@Override
	public Aresta criaAresta2(Aresta aresta) throws TException {
		try{
			lock.lock();
			listaDeArestas = Servidor.carregarArestas(Servidor.mapeamento.getPathBDAresta());
			listaDeArestas.add(aresta);
			Servidor.criaArquivoAresta(listaDeArestas, Servidor.mapeamento.getPathBDAresta());
		}finally{
			lock.unlock();
		}
			
		return aresta;
	}
	

	@Override
	public Aresta deletaAresta(int nomeVertice1, int nomeVertice2) throws TException {
		List<Aresta> listaA = new ArrayList<Aresta>();
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.buscaServidor(nomeVertice2);
			
			if(client == null){
				listaA = Servidor.carregarArestas(Servidor.mapeamento.getPathBDAresta());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			}else{
				return client.deletaAresta(nomeVertice1,nomeVertice2);
			}
			
			for(Aresta a : listaA){
				if((a.getNomeVertice1() == nomeVertice1 && a.getNomeVertice2() == nomeVertice2)
						|| (a.getNomeVertice1() == nomeVertice2 && a.getNomeVertice2() == nomeVertice1)){
					//it.remove();
					listaA.remove(a);
					Servidor.criaArquivoAresta(listaA,Servidor.mapeamento.getPathBDAresta());
					break;
				}
			}
			
			
		}finally{
			lock.unlock();
		}
		
		return new Aresta();
		
	}
	
	
	@Override
	public Aresta deletaAresta2(int nomeVertice1, int nomeVertice2) throws TException {
		List<Aresta> listaA = new ArrayList<Aresta>();
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.buscaServidor(nomeVertice1);
			
			if(client == null){
				listaA = Servidor.carregarArestas(Servidor.mapeamento.getPathBDAresta());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			}else{
				return client.deletaAresta2(nomeVertice1,nomeVertice2);
			}
			
			
			for(Aresta a : listaA){
				if((a.getNomeVertice1() == nomeVertice1 && a.getNomeVertice2() == nomeVertice2)
						|| (a.getNomeVertice1() == nomeVertice2 && a.getNomeVertice2() == nomeVertice1)){
					//it.remove();
					listaA.remove(a);
					Servidor.criaArquivoAresta(listaA,Servidor.mapeamento.getPathBDAresta());
					break;
				}
			}
			
			
			// excluindo a aresta do outro servidor
			client = Servidor.buscaServidor(nomeVertice2);
			client.deletaAresta(nomeVertice1,nomeVertice2);
			
		}finally{
			lock.unlock();
		}
		
		return new Aresta();
		
	}
	
	@Override
	public Aresta deletaAresta3(int nomeVertice) throws TException {
		ArrayList<MapeamentoServidor> listaMapeamento = Servidor.listaMapeamento;
		GrafoService.Client client = null;
		List<Aresta> lista = new ArrayList<Aresta>();
		int porta;
		//Aresta a;
		for(int i = 0; i < listaMapeamento.size(); i++){
			porta = listaMapeamento.get(i).getPorta();
			
			if(porta != Servidor.mapeamento.getPorta()){
				client = Servidor.buscaServidorPorta(porta);
				lista = listaTodasArestas2(porta);
			}else{
				lista = listaTodasArestas();
			}
			
			
			for(Aresta a : lista){
				if((a.getNomeVertice1() == nomeVertice) || (a.getNomeVertice2() == nomeVertice)){
					lista.remove(a);
					break;
				}
			}
			
			if(client !=null){
				client.criaArquivoAresta(lista,listaMapeamento.get(i).getPathBDAresta());
			}
			this.criaArquivoAresta(lista,Servidor.mapeamento.getPathBDAresta());
			
		}
		return null;
	}
	
	@Override
	public void criaArquivoAresta(List<Aresta> lista, String pathBDAresta){
		Servidor.criaArquivoAresta(lista, pathBDAresta);
	}

	@Override
	public List<Vertice> listaTodosVertices() throws TException {
		List<Vertice> lista;
		try{
			lock.lock();
			lista = Servidor.carregarVertices(Servidor.mapeamento.getPathBD());
		}finally{
			lock.unlock();
		}
		return lista;
	}
	

	@Override
	public List<Aresta> listaTodasArestas() throws TException {
		List<Aresta> lista;
		try{
			lock.lock();
			lista = Servidor.carregarArestas(Servidor.mapeamento.getPathBDAresta());
		}finally{
			lock.unlock();
		}
		return lista;
	}
	
	@Override
	public List<Aresta> listaTodasArestas2(int portaServidor) throws TException {
		List<Aresta> lista;
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.simpleClientServidor(portaServidor);
			lista = client.listaTodasArestas();
			
		}finally{
			lock.unlock();
		}
		return lista;
	}

	@Override
	public List<Integer> listaVerticesAdjacentes(int nomeVertice) throws TException {
		
		List<Integer> listaAdjacentes;
		List<Aresta> listaA = new ArrayList<Aresta>();
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.buscaServidor(nomeVertice);
			
			if(client.getInputProtocol() == null){
				listaA = Servidor.carregarArestas(Servidor.mapeamento.getPathBDAresta());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			}else{
				client.listaVerticesAdjacentes(nomeVertice);
			}
		
			listaAdjacentes = new ArrayList<Integer>();
			
			for(Aresta a : listaA){
				if(nomeVertice == a.getNomeVertice1()){
					listaAdjacentes.add(a.getNomeVertice2());
				}
				
				if(nomeVertice == a.getNomeVertice2()){
					listaAdjacentes.add(a.getNomeVertice1());
				}
				
			}
		}finally{
			lock.unlock();
		}
		
		return listaAdjacentes;
	}

	@Override
	public void atualizaVertice(Vertice vertice) throws TException {
		List<Vertice> listaV = new ArrayList<Vertice>();
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.buscaServidor(vertice.getNome());
			
			if(client.getInputProtocol() == null){
				listaV = Servidor.carregarVertices(Servidor.mapeamento.getPathBD());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			}else{
				client.atualizaVertice(vertice);
			}
			
		
			Iterator<Vertice> it = listaV.iterator();
			while(it.hasNext()){
				Vertice a = (Vertice) it.next();
				if(a.getNome() == vertice.getNome()){
					it.remove();
					break;
				}
			}
			
			listaV.add(vertice);
			Servidor.criaArquivoVertice(listaV,Servidor.mapeamento.getPathBD());
		}finally{
			lock.unlock();
		}
		
		
		
		
	}

	@Override
	public void atualizaAresta(Aresta aresta) throws TException {
		List<Aresta> listaA = new ArrayList<Aresta>();
		try{
			lock.lock();
			
			GrafoService.Client client = Servidor.buscaServidor(aresta.getNomeVertice1());
			
			if(client.getInputProtocol() == null){
				listaA = Servidor.carregarArestas(Servidor.mapeamento.getPathBDAresta());
				System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			}else{
				client.atualizaAresta(aresta);
			}
			
			
			Iterator<Aresta> it = listaA.iterator();
			while(it.hasNext()){
				Aresta a = (Aresta) it.next();
				if((a.getNomeVertice1() == aresta.getNomeVertice1()) && (a.getNomeVertice2() == aresta.getNomeVertice2())){
					it.remove();
					break;
				}
			}
			
			listaA.add(aresta);
			aresta.getNomeVertice1();
			//Servidor.criaArquivoAresta(listaA);
		}finally{
			lock.unlock();
		}
	}
	
	
	@Override
	public List<Vertice> buscaTodosVertices() throws TException {
		
		ArrayList<MapeamentoServidor> listaMapeamento = Servidor.listaMapeamento;
		List<Vertice> lista = new ArrayList<Vertice>();
		List<Vertice> listaResultado = new ArrayList<Vertice>();
		String path;
		//Aresta a;
		for(int i = 0; i < listaMapeamento.size(); i++){
			path = listaMapeamento.get(i).getPathBD();
			lista = Servidor.carregarVertices(path);
			listaResultado.addAll(lista);
			lista = new ArrayList<Vertice>();
		}
		
		return listaResultado;
		
	}
	
	@Override
	public List<Aresta> buscaTodasArestas() throws TException {
			
			ArrayList<MapeamentoServidor> listaMapeamento = Servidor.listaMapeamento;
			List<Aresta> lista = new ArrayList<Aresta>();
			List<Aresta> listaResultado = new ArrayList<Aresta>();
			String path;
			//Aresta a;
			for(int i = 0; i < listaMapeamento.size(); i++){
				path = listaMapeamento.get(i).getPathBDAresta();
				lista = Servidor.carregarArestas(path);
				listaResultado.addAll(lista);
				lista = new ArrayList<Aresta>();
			}
			
			return listaResultado;
			
		}
	
	
	public static float[][] converte(List<Aresta> listaArestas, int tamanho){
		float[][] grapho = new float[50][50];
		
		for(Aresta aresta : listaArestas){
			grapho[aresta.getNomeVertice1()][aresta.getNomeVertice2()] = (float) aresta.getPeso();
		}
		
		return grapho;
	}
	
	@Override
	public double menorDistancia(List<Aresta> listaArestas, int tamanho, int tag1, int tag2, List<Vertice> array)	throws TException {
		// TODO Auto-generated method stub
		
		float[][] grapho = converte(listaArestas, array.size());
		Vertice vertice = new Vertice();
		double menorCaminho = 0.0;
		
		
		float distancia[] = new float[tamanho];
		int visitados[] = new int[tamanho];
		int pre[] = new int[tamanho+1];
		float matriz[][] = new float[tamanho][tamanho];
		matriz = grapho;
		for(int i=0; i<=tamanho; i++){
			for(int j=0; j<=tamanho; j++){
				if(matriz[i][j]<=0){
					matriz[i][j]=999;
		        }
		    }
		}
		for(int i=0; i<tamanho; i++){     
			distancia[i] = matriz[tag1][i+1];
		}
		   
		visitados[tag1-1] = 1;
		   
		for(int i=0; i<tamanho; i++){
		//	           min=999;
		//	           for(int j=0; j<tamanho; j++){
		//	               if(min>distancia[j] && visitados[j]!=1){
		//	                   min = distancia[j];
		//	                   next=j;
		//	                   
		//	               }
		//	           }
		   
		   
		   min=999;
		   for(int j=0; j<tamanho; j++){
			   try {
				vertice = buscaVertice(j);
			} catch (TException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	           if(min>distancia[vertice.getNome()] && visitados[vertice.getNome()]!=1){
	               min = distancia[j];
	               next=j;
	               
	           }
	       }
		       
		   
		    visitados[next] = 1;
		    
		    
		    for(int c=0; c<tamanho; c++){
		           if(visitados[c]!=1){
		               if(min+matriz[next+1][c+1]<distancia[c]){
		                   distancia[c]= min+matriz[next+1][c+1];
		                   pre[c] = next;
		               }
		               
		           }
		    }
		   
		    }       
		    int j;
		    if(distancia[tag2-1]==-1 || distancia[tag2-1]==999){
		    	System.out.println("\nCaminho: "+array.get(tag2-1).getNome()+" Distância: "+999);
		}
		else if(distancia[tag2-1]>0 && distancia[tag2-1]<999){
			//System.out.println("\nCaminho: "+array.get(tag2-1).getNome()+" Distância: "+distancia[tag2-1]);
		for(Vertice v : array){
			if(v.getNome() == tag2)
				System.out.println("\nCaminho: de " + tag1 + " até " + v.getNome()+" Distância: "+ distancia[tag2-1]); 
				menorCaminho = distancia[tag2-1];
			}
		}
		j=tag2-1;
		do{
	        j=pre[j];
	        if(j!=0){
	            System.out.println(" <- "+array.get(j).getNome());
	        }
		}while(j!=0);
		
		System.out.println(" <- "+array.get(tag1-1).getNome());
			
			for(int i=0; i<tamanho; i++){     
			    
			    distancia[i] = 0;
			    visitados[i] = 0;
			    pre[i]=0;
			    
			}
			
			
			return menorCaminho;
			
			
	}


	


}