package br.com.SD;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TSSLTransportFactory;
import org.apache.thrift.transport.TSSLTransportFactory.TSSLTransportParameters;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import br.com.SD.RaftCopyCat.CommandCriaVertice;
import br.com.SD.RaftCopyCat.MapStateMachine;
import io.atomix.catalyst.transport.Address;
import io.atomix.catalyst.transport.netty.NettyTransport;
import io.atomix.copycat.client.CopycatClient;
import io.atomix.copycat.server.CopycatServer;
import io.atomix.copycat.server.storage.Storage;
import io.atomix.copycat.server.storage.StorageLevel;
import jxl.read.biff.BiffException;
import br.com.SD.Aresta;
import br.com.SD.GrafoService;
import br.com.SD.GrafoService.Client;
import br.com.SD.GrafoService.Processor;
import br.com.SD.Vertice;

public class Servidor {

	public static Handler handler;
	public static Processor processor;
	public static MapeamentoServidor mapeamento;
	public static ArrayList<MapeamentoServidor> listaMapeamento = new ArrayList<MapeamentoServidor>();
	public static CopycatClient.Builder clientBuilder;
	public static CopycatServer server;
	
	public static void main(String[] args) throws BiffException, IOException {
		
		
		mapeamento = leArquivos("portas").get(0);
		
		try {
			
			//descobrir outros nos deste cluster.
			//crie um cluster copy-cat
			
			// fazer um for PARA ITERAR para achar todos os servidores
			// Se ele form o primeiro controi cluster
			
			Address address = new Address("127.0.0.1", mapeamento.getPorta());
			System.out.println("Porta servidor: " + mapeamento.getPorta());
			CopycatServer.Builder builder = CopycatServer.builder(address);
			
			builder.withStateMachine(MapStateMachine::new);
			
			builder.withTransport(NettyTransport.builder()
			  .withThreads(4)
			  .build());

			builder.withStorage(Storage.builder()
			  .withDirectory(new File("logs"))
			  .withStorageLevel(StorageLevel.DISK)
			  .build());
			//CopycatServer server = builder.build();
			server = builder.build();
			
			server.serializer().register(CommandCriaVertice.class);
			
			//server.bootstrap().join();
			CompletableFuture<CopycatServer> future = server.bootstrap();
			future.join();

				
				
			//else ... demais servidores join no cluster
			
			Iterator<MapeamentoServidor> iter = listaMapeamento.iterator();
			iter.next();
			while(iter.hasNext()){
				Collection<Address> cluster = Collections.singleton(new Address("127.0.0.1", iter.next().getPorta()));
				server.join(cluster).join();
				
			}
			
			
			
			Iterator<MapeamentoServidor> iterClient = listaMapeamento.iterator();
			
			// todos executam
			//veriavel clientBuilder global
			clientBuilder = CopycatClient.builder();
			clientBuilder.withTransport(NettyTransport.builder()
			  .withThreads(2)
			  .build());

			CopycatClient client = clientBuilder.build();
			
			client.serializer().register(CommandCriaVertice.class);
			
			Collection<Address> clusterClient;
			while(iterClient.hasNext()){
				clusterClient = Arrays.asList(
						//endereço dos tres servidores de um cluster
						new Address("127.0.0.1", iterClient.next().getPortaClient())
				);
				
				client.connect(clusterClient).join();
				//CompletableFuture<CopycatClient> futureClient
				//futureClient.join();
			}
			
			
			
			
			handler = new Handler(null);
			processor = new GrafoService.Processor(handler);
	
			Runnable simple = new Runnable() {
				public void run() {
					simple(processor,mapeamento);
				}
			};      
		 
		  new Thread(simple).start();
		} catch (Exception x) {
		  x.printStackTrace();
		}
		
		
	}
	
	
	public static void simple(GrafoService.Processor processor, MapeamentoServidor mapeamento) {
		try {
			
//			TSSLTransportParameters params = new TSSLTransportParameters();
//			// The Keystore contains the private key
//			params.setKeyStore("/home/ruth/SD/thrift-0.10.0/lib/java/test/.keystore", "thrift", null, null);
//			//System.setProperty("https.protocols", "TLSv1.1");
			
			/** Implementando o multi threaded server **/
			TServerTransport serverTransport = new TServerSocket(mapeamento.getPorta());
			TServer server = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));
			//System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
			
			System.out.println("Starting the simple server...");
			server.serve();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public static GrafoService.Client simpleClientServidor(int porta){
		TTransport transport;
		try {
			TSSLTransportParameters params = new TSSLTransportParameters();
			params.setTrustStore("/home/ruth/SD/thrift-0.10.0/lib/java/test/.truststore", "thrift", "SunX509","JKS");
			
			transport = TSSLTransportFactory.getClientSocket("localhost",porta,0,params);
			TProtocol protocol = new  TBinaryProtocol(transport);
			GrafoService.Client client = new GrafoService.Client(protocol);
			return client;
		} catch (TTransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static List<Vertice> carregarVertices(String pathBD){
		System.out.println("Carregando Vertices");
		System.out.println("Servidor: " + mapeamento.getNomeServidor());
		List<Vertice> listaVertices = new LeArquivos().leVertices(pathBD);
		return listaVertices;
	}
	
	public static List<Aresta> carregarArestas(String pathBD){
		List<Aresta> listaArestas = new LeArquivos().carregaArestas(pathBD);
		return listaArestas;
	}
	
	public static void criaArquivoVertice(List<Vertice> lista, String pathBD){
		new LeArquivos().criaArquivoVetice(lista, pathBD);
	}
	
	public static void criaArquivoAresta(List<Aresta> lista, String pathBDAresta){
		new LeArquivos().criaArquivoAresta(lista, pathBDAresta);
	}
	
	public static ArrayList<MapeamentoServidor> leArquivos(String path){
		return new LeArquivos().leArquivos(path);
	}
	
	
	
	public static Client buscaServidor(int nomeVertice){
		int faixa = nomeVertice%mapeamento.getQtdServidores();
		
		if(faixa == mapeamento.getNomeServidor()){
			return null;
		}else{
			
			for(MapeamentoServidor map : listaMapeamento){
				if(map.getNomeServidor() == faixa){
					GrafoService.Client client = simpleClientServidor(map.getPorta());
					return client;
				}
			}
			
			return null;
		}		
				
	}
	
	public static Client buscaServidorPorta(int porta){
		GrafoService.Client client = simpleClientServidor(porta);
		return client;
	}
	
	
	
	
}