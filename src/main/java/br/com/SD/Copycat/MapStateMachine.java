package br.com.SD.Copycat;

import java.util.HashMap;
import java.util.Map;

import io.atomix.copycat.server.Commit;
import io.atomix.copycat.server.Snapshottable;
import io.atomix.copycat.server.StateMachine;
import io.atomix.copycat.server.storage.snapshot.SnapshotReader;
import io.atomix.copycat.server.storage.snapshot.SnapshotWriter;

public class MapStateMachine extends StateMachine implements Snapshottable {
	
	private Map<Object, Object> map = new HashMap<>();

	  public Object put(Commit<PutCommand> commit) {
	    try {
	      map.put(commit.operation().key(), commit.operation().value());
	    } finally {
	      commit.close();
	    }
		return commit;
	  }

	  public Object get(Commit<GetQuery> commit) {
	    try {
	      return map.get(commit.operation().key());
	    } finally {
	      commit.close();
	    }
	  }

	@Override
	public void install(SnapshotReader reader) {
		map = reader.readObject();
		
	}

	@Override
	public void snapshot(SnapshotWriter writer) {
		writer.writeObject(map);		
	}
	
}