package br.com.SD.Copycat;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;

import io.atomix.catalyst.transport.Address;
import io.atomix.catalyst.transport.netty.NettyTransport;
import io.atomix.copycat.client.CopycatClient;

public class AppClient {
	
	public static void main (String[] args){
		
		CopycatClient.Builder builder = CopycatClient.builder();
		
		builder.withTransport(NettyTransport.builder()
				  .withThreads(2)
				  .build());

		//CopycatClient client = builder.build();
//		CopycatClient client = CopycatClient.builder(members)
//				  .withTransport(NettyTransport.builder()
//				    .withThreads(2)
//				    .build())
//				  .build();
//		
		//client.serializer().register(PutCommand.class);
		//client.serializer().register(GetQuery.class);
		
		
		CopycatClient client = builder.build();
		Collection<Address> cluster = Arrays.asList(
		  new Address("127.0.0.1", 5000),
		  new Address("127.0.0.1", 8700),
		  new Address("127.0.0.1", 9700)
		);

		CompletableFuture<CopycatClient> future1 = client.connect(cluster);
		future1.join();
		
		CompletableFuture<Object> future = client.submit(new PutCommand("foo", "Hello world!"));
		Object result = future.join();
		
		
	}

}
