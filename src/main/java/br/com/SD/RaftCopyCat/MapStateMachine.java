package br.com.SD.RaftCopyCat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.SD.Servidor;
import io.atomix.copycat.server.Commit;
import io.atomix.copycat.server.StateMachine;
import br.com.SD.Vertice;

public class MapStateMachine extends StateMachine {
	private Map<Object, Object> map = new HashMap<>();
	
	public Object put(Commit<CommandCriaVertice> commit) {
		try {
			List<Vertice> listaV= new ArrayList<Vertice>();
			listaV = Servidor.carregarVertices(Servidor.mapeamento.getPathBD());
			System.out.println("Identificador servidor: " + Servidor.mapeamento.getNomeServidor());
			for(Vertice v : listaV){
				if(commit.operation().getVertice().getNome() == v.getNome()){
					System.out.println("Já existe um vertice com este nome");
					return new Vertice();
				}
			}
			listaV.add(commit.operation().getVertice());
			Servidor.criaArquivoVertice(listaV,Servidor.mapeamento.getPathBD());
			
			return commit.operation().getVertice();
			
	    } finally {
	      commit.close();
	    }
	 }

	 public Object get(Commit<GetQuery> commit) {
	    try {
	      return map.get(commit.operation().key());
	    } finally {
	      commit.close();
	    }
	 }
	
}