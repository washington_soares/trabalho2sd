package br.com.SD.RaftCopyCat;

import io.atomix.copycat.Command;
import br.com.SD.Vertice;

public class CommandCriaVertice implements Command<Vertice> {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final Vertice vertice;

	public CommandCriaVertice(Vertice vertice) {
		System.out.println("Testando CommandCriaVertice");
		this.vertice = vertice;
	}

	public Vertice getVertice(){
		return this.vertice;
	}
	
}